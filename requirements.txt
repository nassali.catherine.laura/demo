asgiref==3.7.2
Django==5.0.2
sqlparse==0.4.4
tzdata==2024.1
dj_database_url==2.1
coverage==7.4.4
gunicorn==21.2.0
django_environ
psycopgy2-binary
pillow==10.2.0
whitenoise

